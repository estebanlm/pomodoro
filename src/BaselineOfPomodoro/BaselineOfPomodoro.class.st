Class {
	#name : #BaselineOfPomodoro,
	#superclass : #BaselineOf,
	#category : #BaselineOfPomodoro
}

{ #category : #baselines }
BaselineOfPomodoro >> baseline: spec [
	<baseline>
	
	spec for: #common do: [
		self objCBridge: spec.
		self voyage: spec.
		spec 
			package: 'LED';
			package: 'Pomodoro' with: [ spec requires: #(Voyage ObjCBridge LED) ] ].
]

{ #category : #baselines }
BaselineOfPomodoro >> objCBridge: spec [
	spec 
		baseline: 'ObjCBridge'
		with: [ 
			spec repository: 'github://estebanlm/objcbridge/src' ]
]

{ #category : #baselines }
BaselineOfPomodoro >> voyage: spec [
	spec 
		baseline: 'Voyage'
		with: [ 
			spec 
				repository: 'github://pharo-nosql/voyage/mc';
				loads: 'unqlite tests' ]
]

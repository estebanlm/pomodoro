"
I store metadata for this package. These meta data are used by other tools such as the SmalllintManifestChecker and the critics Browser
"
Class {
	#name : #ManifestLED,
	#superclass : #PackageManifest,
	#category : #LED
}

{ #category : #meta-data }
ManifestLED >> description [
	^ 'I am a collection of LED digits that can display a decimal value.  The display can be set to flash by sending flash: true.

LedMorph can now display characters:

	LedMorph new  string:''0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ''; openInWorld
'
]

{ #category : #meta-data }
ManifestLED >> file [
	^ 'LED-StephaneDucasse.5'
]

{ #category : #meta-data }
ManifestLED >> packageName [
	^ #LED
]

{ #category : #meta-data }
ManifestLED >> repositories [
	^ #('http://smalltalkhub.com/mc/PharoExtras/LEDMorph/main/')
]

{ #category : #meta-data }
ManifestLED >> version [
	^ '1.0'
]

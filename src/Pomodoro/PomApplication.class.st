"
I'm a pomodoro application.

self new run.
"
Class {
	#name : #PomApplication,
	#superclass : #CocoaApplication,
	#instVars : [
		'announcer',
		'statusBarItem',
		'statusBarMenu',
		'resourcesBundle',
		'preferences',
		'timerWindowController',
		'preferencesViewController'
	],
	#category : #Pomodoro-View
}

{ #category : #announce }
PomApplication >> announce: anAnnouncement [
	self announcer announce: anAnnouncement
]

{ #category : #announce }
PomApplication >> announcer [
	^ announcer ifNil: [ announcer := Announcer new ]
]

{ #category : #'private factory' }
PomApplication >> createPreferencesViewController [
	^ PomPreferencesViewController on: self
]

{ #category : #'private factory' }
PomApplication >> createStatusMenu [
	^ PomStatusMenu new
		application: self;
		yourself
]

{ #category : #'private factory' }
PomApplication >> createTimerWindowController [
	^ PomTimerWindowController on: self
		
]

{ #category : #initialization }
PomApplication >> initialize [
	super initialize.
	self announcer
		when: PomTimerWillStart send: #timerWillStart: to: self;
		when: PomTimerTick send: #timerTick: to: self;
		when: PomTimerDidStop send: #timerDidStop: to: self.		
]

{ #category : #private }
PomApplication >> installStatusMenu [
	| bar |

	bar := #NSStatusBar inObjC systemStatusBar.
	statusBarMenu := self createStatusMenu.
	(statusBarItem := bar statusItemWithLength: NSVariableStatusItemLength)
		setTitle: statusBarMenu proxy title;
		setHighlightMode: true;
		setMenu: statusBarMenu proxy; 
		retain; "do not release now, I will release when no longer referenced in Pharo"
		autoRelease. "release when not referenced in Pharo"
]

{ #category : #actions }
PomApplication >> openPreferences [
	(#NSWindow inObjC windowWithContentViewController: self preferencesViewController proxy)
		setTitle: 'Preferences...' asNSString autorelease;
		makeKeyAndOrderFront: ObjCObject nil;
		autoRelease
]

{ #category : #accessing }
PomApplication >> preferences [
	^ preferences ifNil: [ preferences := PomPreferences retrieve ]
]

{ #category : #'private accessing' }
PomApplication >> preferencesViewController [
	^ preferencesViewController ifNil: [ 
		preferencesViewController := self createPreferencesViewController ].
]

{ #category : #accessing }
PomApplication >> reset [ 
	self uninstallStatusMenu
]

{ #category : #private }
PomApplication >> resetStatusMenu [
	statusBarMenu := self createStatusMenu.
	statusBarItem setMenu: statusBarMenu proxy
]

{ #category : #accessing }
PomApplication >> resourcesBundle [
	^ resourcesBundle ifNil: [ | path |
		path := self resourcesBundlePath asNSString autorelease.
		resourcesBundle := (#NSBundle inObjC bundleWithPath: path) autoRelease ]
]

{ #category : #accessing }
PomApplication >> resourcesBundlePath [
	^ 'PomodoroResources/Build/Products/Debug/PomodoroResources.bundle' asFileReference fullName
]

{ #category : #running }
PomApplication >> run [
	super run.
	self installStatusMenu
]

{ #category : #'event handling' }
PomApplication >> timerDidStop: ann [
]

{ #category : #'event handling' }
PomApplication >> timerTick: ann [
	statusBarItem setTitle: ann timer minutes MMSS asNSString autorelease
]

{ #category : #'event handling' }
PomApplication >> timerWillStart: ann [
]

{ #category : #'private accessing' }
PomApplication >> timerWindowController [
	^ timerWindowController ifNil: [ 
		timerWindowController := self createTimerWindowController ]
]

{ #category : #actions }
PomApplication >> toggleTimerWindow [
	| window |
	window := self timerWindowController proxy window.
	window isVisible = (Character value: 1)
		ifTrue: [ 
			window close.
			self announce: PomTimerWindowDeactivated ]
		ifFalse: [
			window contentView setNeedsDisplay: true. 
			window makeKeyAndOrderFront: ObjCObject nil.
			self announce: PomTimerWindowActivated ]
]

{ #category : #private }
PomApplication >> uninstallStatusMenu [
	#NSStatusBar inObjC systemStatusBar removeStatusItem: statusBarItem 
]

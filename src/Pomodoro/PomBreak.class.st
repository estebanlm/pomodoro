"
I'm a break lapse.
"
Class {
	#name : #PomBreak,
	#superclass : #PomType,
	#category : #Pomodoro-Model
}

{ #category : #accessing }
PomBreak >> minutes [
	^ self application preferences break
]

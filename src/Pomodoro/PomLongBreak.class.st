"
I'm a long break lapse.
"
Class {
	#name : #PomLongBreak,
	#superclass : #PomType,
	#category : #Pomodoro-Model
}

{ #category : #accessing }
PomLongBreak >> minutes [
	^ self application preferences longBreak
]

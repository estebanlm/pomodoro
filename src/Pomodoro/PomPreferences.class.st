"
I'm a container for the user preferences. 
I keep my data in disk and retrieve it each time the app needs something.
"
Class {
	#name : #PomPreferences,
	#superclass : #Object,
	#instVars : [
		'showSeconds',
		'work',
		'break',
		'longBreak'
	],
	#category : #Pomodoro-Model
}

{ #category : #'instance creation' }
PomPreferences class >> new [
	self error: 'Use #retrieve'
]

{ #category : #'instance creation' }
PomPreferences class >> retrieve [
	| ref |
	self flag: #todo. "From user data when in production"
	ref := 'preferences.ston' asFileReference.
	ref exists ifFalse: [ ^ super new ].
	^ ref readStreamDo: [ :stream |
		STON fromStream: stream ]
	
]

{ #category : #accessing }
PomPreferences >> break [
	^ break
]

{ #category : #accessing }
PomPreferences >> break: anObject [
	break := anObject
]

{ #category : #initialization }
PomPreferences >> initialize [
	super initialize.
	self showSeconds: true.
	self work: 25.
	self break: 5.
	self longBreak: 15
]

{ #category : #testing }
PomPreferences >> isShowSeconds [
	^ showSeconds
]

{ #category : #accessing }
PomPreferences >> longBreak [
	^ longBreak
]

{ #category : #accessing }
PomPreferences >> longBreak: anObject [
	longBreak := anObject
]

{ #category : #accessing }
PomPreferences >> showSeconds [
	^ showSeconds
]

{ #category : #accessing }
PomPreferences >> showSeconds: anObject [
	showSeconds := anObject
]

{ #category : #accessing }
PomPreferences >> work [
	^ work
]

{ #category : #accessing }
PomPreferences >> work: anObject [
	work := anObject
]

Class {
	#name : #PomPreferencesViewController,
	#superclass : #CocoaViewController,
	#instVars : [
		'application',
		'textFieldWork',
		'textFieldBreak',
		'textFieldLongBreak'
	],
	#category : #Pomodoro-View
}

{ #category : #'instance creation' }
PomPreferencesViewController class >> new [
	self error: 'Use #on:'
]

{ #category : #private }
PomPreferencesViewController class >> objCSuperclassName [
	^ 'NSViewController'
]

{ #category : #'instance creation' }
PomPreferencesViewController class >> on: anApplication [
	^ self basicAlloc
		initializeApplication: anApplication;
		yourself
]

{ #category : #accessing }
PomPreferencesViewController >> application [
	^ application
]

{ #category : #initialization }
PomPreferencesViewController >> initializeApplication: anApplication [
	application := anApplication.
	self initialize
]

{ #category : #initialization }
PomPreferencesViewController >> initializeProxy [
	| view preferences |

	self proxy 
		initWithNibName: 'PreferencesView' asNSString autorelease
		bundle: self application resourcesBundle.
	view := self proxy view.
	self flag: #todo. "This is not the best (look for tag), but I need to see why find by 
	identifier is not working"
	textFieldWork := self subviewWithTag: 10 in: view.
	textFieldBreak := self subviewWithTag: 20 in: view.
	textFieldLongBreak := self subviewWithTag: 30 in: view.
	
	preferences := self application preferences.
	textFieldWork setIntegerValue: preferences work.
	textFieldBreak setIntegerValue: preferences break.
	textFieldLongBreak setIntegerValue: preferences longBreak.
]

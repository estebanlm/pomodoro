"
I'm a status bar menu to show pomodoro options in the status bar.
I also show the timer there.
"
Class {
	#name : #PomStatusMenu,
	#superclass : #Object,
	#traits : 'TObjCProxyClass',
	#classTraits : 'TObjCProxyClass classTrait',
	#instVars : [
		'proxy',
		'application',
		'stopMenu',
		'toggleWindowMenu',
		'timer'
	],
	#category : #Pomodoro-View
}

{ #category : #accessing }
PomStatusMenu class >> labelHideClockWindow [
	^ 'Hide Timer'
]

{ #category : #accessing }
PomStatusMenu class >> labelNoActiveTimer [
	^ 'Not Started'
]

{ #category : #accessing }
PomStatusMenu class >> labelShowClockWindow [
	^ 'Show Timer'
]

{ #category : #private }
PomStatusMenu class >> objCSuperclassName [
	^ 'NSMenu'
]

{ #category : #accessing }
PomStatusMenu >> application [
	^ application
]

{ #category : #accessing }
PomStatusMenu >> application: anApplication [
	application == anApplication ifTrue: [ ^ self ].
	application ifNotNil: [ application announcer unsubscribe: self ].
	application := anApplication.
	self subscribeToNotifications
]

{ #category : #private }
PomStatusMenu >> finalizeTimer [
	(timer isNil or: [ timer isStopped ]) ifTrue: [ ^ self ].
	timer stop
]

{ #category : #initialization }
PomStatusMenu >> initialize [
	super initialize.
	self initializeProxy
]

{ #category : #initialization }
PomStatusMenu >> initializeProxy [
	self proxy initWithTitle: '00:00' asNSString autorelease.
	self proxy addItem: ((stopMenu := #NSMenuItem inObjC alloc)
		initWithTitle: self class labelNoActiveTimer asNSString autorelease 
			action: #menuStop: asObjCSelector
			keyEquivalent: '' asNSString autorelease;
		setTarget: self proxy;
		setEnabled: false;
		yourself).
	self proxy addItem: ((toggleWindowMenu := #NSMenuItem inObjC alloc)
		initWithTitle: self class labelShowClockWindow asNSString autorelease 
			action: #menuToggleWindow: asObjCSelector
			keyEquivalent: '' asNSString autorelease;
		setTarget: self proxy;
		yourself).
	self proxy addItem: (#NSMenuItem inObjC separatorItem).
	self proxy addItem: (#NSMenuItem inObjC alloc 
		initWithTitle: 'Start' asNSString autorelease 
			action: #menuStart: asObjCSelector
			keyEquivalent: '' asNSString autorelease;
		setTarget: self proxy;
		yourself).
	self proxy addItem: (#NSMenuItem inObjC alloc
		initWithTitle: 'Break' asNSString autorelease 
			action: #menuBreak: asObjCSelector
			keyEquivalent: '' asNSString autorelease;
		setTarget: self proxy;
		yourself).
	self proxy addItem: (#NSMenuItem inObjC alloc
		initWithTitle: 'Long break' asNSString autorelease 
			action: #menuLongBreak: asObjCSelector
			keyEquivalent: '' asNSString autorelease;
		setTarget: self proxy;
		yourself).
	self proxy addItem: (#NSMenuItem inObjC separatorItem).
	self proxy addItem: (#NSMenuItem inObjC alloc
		initWithTitle: 'Preferences...' asNSString autorelease 
			action: #menuPreferences: asObjCSelector
			keyEquivalent: '' asNSString autorelease;
		setTarget: self proxy;
		yourself).
	self proxy addItem: (#NSMenuItem inObjC separatorItem).
	self proxy addItem: (#NSMenuItem inObjC alloc
		initWithTitle: 'Quit Pomodoro' asNSString autorelease 
			action: #menuQuit: asObjCSelector
			keyEquivalent: '' asNSString autorelease;
		setTarget: self proxy;
		yourself)
]

{ #category : #testing }
PomStatusMenu >> isTimerRunning [
	^ timer notNil 
		and: [ timer isStopped not ]
]

{ #category : #'menu actions' }
PomStatusMenu >> menuBreak: aNotification [
	<objCSignature: #(void (id aNotification))>

	self finalizeTimer.
	(timer := PomType break newTimer) start.	
]

{ #category : #'menu actions' }
PomStatusMenu >> menuLongBreak: aNotification [
	<objCSignature: #(void (id aNotification))>
	
	self finalizeTimer.
	(timer := PomType longBreak newTimer) start.	
]

{ #category : #'menu actions' }
PomStatusMenu >> menuPreferences: aNotification [
	<objCSignature: #(void (id aNotification))>
	self application openPreferences
]

{ #category : #'menu actions' }
PomStatusMenu >> menuQuit: aNotification [
	<objCSignature: #(void (id aNotification))>
]

{ #category : #'menu actions' }
PomStatusMenu >> menuStart: aNotification [
	<objCSignature: #(void (id aNotification))>
	
	self finalizeTimer.
	(timer := PomType work newTimer) start.
]

{ #category : #'menu actions' }
PomStatusMenu >> menuStop: aNotification [
	<objCSignature: #(void (id aNotification))>
	self timer ifNil: [ ^ self ].
	self timer stop.
]

{ #category : #'menu actions' }
PomStatusMenu >> menuToggleWindow: aNotification [
	<objCSignature: #(void (id aNotification))>

	self application toggleTimerWindow
]

{ #category : #accessing }
PomStatusMenu >> proxy [
	^ proxy
]

{ #category : #accessing }
PomStatusMenu >> proxy: anObjCObject [
	proxy := anObjCObject
]

{ #category : #private }
PomStatusMenu >> subscribeToNotifications [
	self application announcer
		when: PomTimerWillStart send: #timerWillStart: to: self;
		when: PomTimerDidStop send: #timerDidStop: to: self;
		when: PomTimerWindowActivated send: #timerWindowActivated: to: self;
		when: PomTimerWindowDeactivated send: #timerWindowDeactivated: to: self
]

{ #category : #accessing }
PomStatusMenu >> timer [
	^ timer
]

{ #category : #'event handling' }
PomStatusMenu >> timerDidStop: ann [
	stopMenu setTitle: self class labelNoActiveTimer asNSString autorelease
]

{ #category : #'event handling' }
PomStatusMenu >> timerWillStart: ann [
	stopMenu setTitle: 'Stop' asNSString autorelease
]

{ #category : #'event handling' }
PomStatusMenu >> timerWindowActivated: ann [
	toggleWindowMenu setTitle: self class labelHideClockWindow asNSString autorelease
]

{ #category : #'event handling' }
PomStatusMenu >> timerWindowDeactivated: ann [
	toggleWindowMenu setTitle: self class labelShowClockWindow asNSString autorelease
]

{ #category : #'menu actions' }
PomStatusMenu >> validateMenuItem: menuItem [
	<objCSignature: #(BOOL (id menuItem))>
	<objCOnErrorReturn: false>

	menuItem = stopMenu getHandle asExternalAddress
		ifTrue: [ ^ self isTimerRunning ].
	^ true
]

"
I'm a timer for Pomodoro. 
I use the function time() from system libraries instead one of Pharo primitives just to make sure we have the accurate time.

Each time an user starts a timer (work, break, long break) he calls me to process the ticks. 
"
Class {
	#name : #PomTimer,
	#superclass : #Object,
	#traits : 'TPomModel',
	#classTraits : 'TPomModel classTrait',
	#instVars : [
		'type',
		'minutes',
		'stopped'
	],
	#category : #Pomodoro-Model
}

{ #category : #'instance creation' }
PomTimer class >> new [
	self error: 'Use #type:minutes:'
]

{ #category : #'instance creation' }
PomTimer class >> type: aType minutes: aNumber [
	^ self basicNew 
		initializeType: aType minutes: aNumber;
		yourself
]

{ #category : #initialization }
PomTimer >> initializeType: aType minutes: aNumber [
	self initialize.
	type := aType.
	minutes := aNumber minutes.
	stopped := true.	

]

{ #category : #testing }
PomTimer >> isStopped [
	^ stopped
]

{ #category : #accessing }
PomTimer >> minutes [
	^ minutes
]

{ #category : #accessing }
PomTimer >> start [
	| started |
	self application announce: (PomTimerWillStart timer: self).
	stopped := false.
	"This is important: Pharo runs on the main thread (it shouldn't, but it does), which means 
	 all the time spent on the UI (for example, interacting with the menu), the image will be 
	 freezed and then the simple countdown will not work I have to update against the time 
	 to know how much time passed.
	 Event worst. DateAndTime is not correct... if the image is frozen it will delay the 
	 internal counter and we need to do a reliquish"
	started := self time.	
	[ [ stopped not and: [ minutes isZero not ] ]
	whileTrue: [ | lastTime passed |
		lastTime := started.
		passed := ((started := self time) - lastTime) seconds.
		minutes := minutes - passed.
		1 second wait.
		self application announce: (PomTimerTick timer: self) ].
	self application announce: (PomTimerDidStop timer: self) ]
	fork
]

{ #category : #accessing }
PomTimer >> stop [
	stopped := true
]

{ #category : #accessing }
PomTimer >> time [
	^ self ffiCall: #(int time()) module: LibC
]

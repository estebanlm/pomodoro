"
I'm a generic timer announcement.
"
Class {
	#name : #PomTimerAnnouncement,
	#superclass : #Announcement,
	#instVars : [
		'timer'
	],
	#category : #Pomodoro-Model
}

{ #category : #'instance creation' }
PomTimerAnnouncement class >> timer: aTimer [
	^ self new timer: aTimer
]

{ #category : #accessing }
PomTimerAnnouncement >> timer [
	^ timer
]

{ #category : #accessing }
PomTimerAnnouncement >> timer: anObject [
	timer := anObject
]

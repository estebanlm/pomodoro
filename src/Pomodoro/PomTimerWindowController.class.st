"
I'm a window controller (wrapper of NSWindowController) who will handle the timer window, when it is open.
"
Class {
	#name : #PomTimerWindowController,
	#superclass : #CocoaWindowController,
	#instVars : [
		'application',
		'ledView',
		'window',
		'ledMorph'
	],
	#category : #Pomodoro-View
}

{ #category : #'instance creation' }
PomTimerWindowController class >> on: anApplication [
	^ self basicAlloc
		initializeApplication: anApplication;
		yourself
]

{ #category : #accessing }
PomTimerWindowController >> application [
	^ application
]

{ #category : #private }
PomTimerWindowController >> application: anApplication [ 
	application == anApplication ifTrue: [ ^ self ].
	application ifNotNil: [ application announcer unsubscribe: self ].
	application := anApplication.
	self subscribeToNotifications
]

{ #category : #accessing }
PomTimerWindowController >> createLedView [
	| view |
	view := CocoaMorphView new.
	view morph: (ledMorph := LedMorph new 
		digits: 5;
		string: '00:00').
	^ view
	
]

{ #category : #initialization }
PomTimerWindowController >> initializeApplication: anApplication [
	self application: anApplication.
	self initialize
]

{ #category : #initialization }
PomTimerWindowController >> initializeProxy [
	| screen screenSize winSize position |

	self proxy init.
	self application resourcesBundle
		loadNibNamed: 'TimerWindow' asNSString autorelease
		owner: self proxy
		topLevelObjects: ExternalAddress null.
	window := self proxy window.
	window setDelegate: self proxy.
	
	screen := #NSScreen inObjC mainScreen.
	screenSize := screen frame asRectangle extent.
	winSize := window frame asRectangle extent.
	position := screenSize - winSize. "top right"
	"position := (screenSize - winSize) x@0." "bottom right" 
	window
		setFrame: (position extent: winSize) asNSRect 
		display: true.
	window setContentView: self ledView proxy
]

{ #category : #accessing }
PomTimerWindowController >> ledView [
	^ ledView ifNil: [ ledView := self createLedView ]
]

{ #category : #private }
PomTimerWindowController >> subscribeToNotifications [
	self application announcer
		when: PomTimerTick 
		send: #timerTick: 
		to: self

	
]

{ #category : #'event handling' }
PomTimerWindowController >> timerTick: ann [
	ledMorph string: (ann timer minutes MMSS padRightTo: 10).
	proxy window contentView setNeedsDisplay: true
]

{ #category : #delegate }
PomTimerWindowController >> windowWillClose: aNotification [
	<objCSignature: #(void (id aNotification))>

	self application announce: PomTimerWindowDeactivated
]

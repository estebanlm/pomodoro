"
I'm a  generic type of pomodoro lapse.
"
Class {
	#name : #PomType,
	#superclass : #Object,
	#traits : 'TPomModel',
	#classTraits : 'TPomModel classTrait',
	#classInstVars : [
		'uniqueInstance'
	],
	#category : #Pomodoro-Model
}

{ #category : #accessing }
PomType class >> break [
	^ PomBreak uniqueInstance
]

{ #category : #accessing }
PomType class >> longBreak [
	^ PomLongBreak uniqueInstance
]

{ #category : #'instance creation' }
PomType class >> new [
	^ self error: 'Use #uniqueInstance'
]

{ #category : #private }
PomType class >> uniqueInstance [
	self = PomType ifTrue: [ 
		self error: 'I''m an abstract class. Use one of my children.' ].
	^ uniqueInstance ifNil: [ uniqueInstance := self basicNew initialize ]
]

{ #category : #accessing }
PomType class >> work [
	^ PomWork uniqueInstance
]

{ #category : #accessing }
PomType >> minutes [
	^ self subclassResponsibility
]

{ #category : #factory }
PomType >> newTimer [
	^ PomTimer 
		type: self 
		minutes: self minutes
]

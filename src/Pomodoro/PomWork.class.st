"
I'm a work lapse.
"
Class {
	#name : #PomWork,
	#superclass : #PomType,
	#category : #Pomodoro-Model
}

{ #category : #accessing }
PomWork >> minutes [
	^ self application preferences work
]
